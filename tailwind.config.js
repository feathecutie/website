const { addDynamicIconSelectors } = require("@iconify/tailwind");
const defaultTheme = require("tailwindcss/defaultTheme");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["**/src/**/*.{html,rs}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ['"Quicksand Variable"', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [
    require("daisyui"),
    addDynamicIconSelectors({
      scale: 1.4,
    }),
  ],
  daisyui: {
    themes: [
      {
        // mytheme: {
        //   primary: "#9b00ff",
        //   secondary: "#1c5aff",
        //   accent: "#00a400",
        //   neutral: "#1e1421",
        //   "base-100": "#382433",
        //   info: "#00d3ff",
        //   success: "#009e3d",
        //   warning: "#c42200",
        //   error: "#ed0241",
        // },
        mytheme2: {
          primary: "#ea6478",
          secondary: "#84cc16",
          accent: "#a93700",
          neutral: "#2c141c",
          "base-100": "#fef3c7",
          // "base-200": "#f1954a",
          // "base-300": "#9b6d4a",
          info: "#0079fa",
          success: "#a6d200",
          warning: "#fc5400",
          error: "#ca003a",
        },
      },
      // "retro",
    ],
    // darkTheme: "coffee",
  },
};

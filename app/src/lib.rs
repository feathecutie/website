use crate::error_template::{AppError, ErrorTemplate};

use leptos::*;
use leptos_meta::*;
use leptos_router::*;
use rand::prelude::*;
use wasm_bindgen::prelude::*;
use web_sys::Event;

pub mod error_template;

/// Main component of the app, use for mounting to body
#[component]
pub fn App() -> impl IntoView {
    // Provides context that manages stylesheets, titles, meta tags, etc.
    provide_meta_context();

    view! {
        <Stylesheet id="leptos" href="/pkg/feas-website.css"/>

        // sets the document title
        <Title text="fea's corner of the internet"/>

        <Link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
        <Link rel="icon" type_="image/png" sizes="32x32" href="/favicon-32x32.png"/>
        <Link rel="icon" type_="image/png" sizes="16x16" href="/favicon-16x16.png"/>
        <Link rel="manifest" href="/site.webmanifest"/>

        // content for this welcome page
        <Router fallback=|| {
            let mut outside_errors = Errors::default();
            outside_errors.insert_with_default_key(AppError::NotFound);
            view! { <ErrorTemplate outside_errors/> }.into_view()
        }>
            <noscript>
                <div class="bg-base-200 p-2">
                    <span>
                        "hey, looks like you have javascript disabled!
                        that's fine, this site is designed to work without javascript, but there are a few things that might be a bit more fun with scripting turned on.
                        it's your choice, feel free to leave it disabled!
                        and if you feel like looking at the code before allowing any random scripting (especially WASM, since this site is written in rust),
                        my site is open source: "
                        <A href="https://codeberg.org/feathecutie/website">
                            "https://codeberg.org/feathecutie/website"
                        </A>
                    </span>
                </div>
            </noscript>
            <main class="container mx-auto p-4">
                <Routes>
                    <Route path="" view=HomePage/>
                </Routes>
            </main>
        </Router>
    }
}

#[component]
fn A(
    #[prop(into)] href: MaybeSignal<String>,
    #[prop(optional)]
    #[prop(into)]
    rel: Option<MaybeSignal<String>>,
    #[prop(optional)]
    #[prop(into)]
    title: MaybeSignal<String>,
    #[prop(optional)]
    #[prop(into)]
    icon: Option<MaybeSignal<String>>,
    children: Children,
) -> impl IntoView {
    view! {
        <a
            href=href
            class="link hover:text-primary focus:text-primary hover:font-bold focus:font-bold"
            rel=rel
            target="_blank"
            title=title
        >
            {icon
                .map(|i| {
                    view! {
                        <span class=move || {
                            format!("{} text-primary inline-block align-middle mr-1", i())
                        }></span>
                    }
                })}

            {children()}
        </a>
    }
}

#[component]
fn Menu(
    #[prop(into)] title: String,
    children: Children,
    #[prop(optional)]
    #[prop(into)]
    href: Option<String>,
) -> impl IntoView {
    let children = children()
        .nodes
        .into_iter()
        .map(|child| view! { <li>{child}</li> })
        .collect_view();

    view! {
        <h2 class="font-bold text-base-content/60 pb-0.5">
            {match href {
                Some(url) => view! { <A href=url>{title}</A> }.into_view(),
                None => title.into_view(),
            }}

        </h2>

        <ul class="border-secondary border-l-2 ml-1 pl-2 mb-1">{children}</ul>
    }
}

#[island]
fn QuantumPoem(rows: [String; 4]) -> impl IntoView {
    let (rows, set_rows) = create_signal(rows);

    create_effect(move |_| {
        document().set_onvisibilitychange(Some(
            Closure::<dyn Fn(Event)>::new(move |_: Event| {
                if document().hidden() {
                    set_rows.update(|rows| {
                        let prev = rows.clone();
                        while prev == *rows {
                            rows.shuffle(&mut thread_rng())
                        }
                    });
                }
            })
            .into_js_value()
            .unchecked_ref(),
        ));
    });

    view! {
        <ul>
            {move || {
                rows()
                    .into_iter()
                    .map(|child| {
                        view! {
                            <li class="p-0.5 font-semibold font-serif">
                                <span class="bg-base-200 px-1 py-0.5">{child}</span>
                            </li>
                        }
                    })
                    .collect_view()
            }}

        </ul>
    }
}

/// Renders the home page of your application.
#[component]
fn HomePage() -> impl IntoView {
    view! {
        <h1 class="text-2xl font-bold text-primary">"fea' s corner of the internet"</h1>
        <p>"hey! i'm fea, 21y/o and.. i don't really know what to say about myself"</p>
        <div>
            <Menu title="pronouns">"she/they"</Menu>
            <Menu title="socials & contact">
                <A
                    href="https://eldritch.cafe/@feathecutie"
                    rel="me"
                    title="Mastodon"
                    icon="icon-[tabler--brand-mastodon]"
                >
                    "@feathecutie@eldritch.cafe"
                </A>
                <A
                    href="https://matrix.to/#/@feathecutie:tchncs.de"
                    title="Matrix"
                    icon="icon-[tabler--brand-matrix]"
                >
                    "@feathecutie:tchncs.de"
                </A>
                <A
                    href="https://feathecutie.tumblr.com"
                    title="tumblr"
                    icon="icon-[tabler--brand-tumblr]"
                >
                    "feathecutie"
                </A>
                <>
                    <A href="mailto:mails@feathecutie.de" title="e-mail" icon="icon-[tabler--mail]">
                        "mails@feathecutie.de"
                    </A>
                    " (I will setup PGP soon(? maybe), but I don't read my emails much anyways, so if possible I'd prefer to be contacted via Matrix (especially if we know each other))"
                </>
                <A
                    href="https://codeberg.org/feathecutie"
                    title="git-hosting"
                    icon="icon-[tabler--brand-git]"
                >
                    "codeberg.org/feathecutie"
                </A>
            </Menu>
            <Menu title="stuff i made">
                <Menu title="queerka.feathecutie.de" href="https://queerka.feathecutie.de">
                    <span>
                        "parses the rss feed of " <A href="https://queerka.de">queerka.de</A>
                        " into iCal for calenders that support webcal to consume"
                    </span>
                    <A href="https://codeberg.org/feathecutie/queerka-webcal">
                        "source code hosted here"
                    </A>
                </Menu>
                <Menu title="my NixOS flake" href="https://codeberg.org/feathecutie/flake">
                    "the flake deployed on all my NixOS hosts"
                    "criticism is welcome^^ especially if i ever put something private in there that i really shouldn't"
                </Menu>
                <Menu title="this website">
                    "yea.. obviously"
                    <A href="https://codeberg.org/feathecutie/website">"source code hosted here"</A>
                </Menu>
                <Menu
                    title="crochet simulation thingy whatever"
                    href="https://codeberg.org/feathecutie/crochet"
                >
                    "this is extremely unfinished but i mess around with it from time to time"
                    "basically tries to read textual crochet pattern (in a specific syntax i made up)
                    and then tries to simulate them into their basic 3d structure"
                    "this doesn't simulate the actual physics of the yarn and 'knots' bc that sounds incredibly hard to do,
                    instead i just simplify each crochet stitch as a simple node in 3d space"
                    "once again, this is extremely unfinished (and unfished, that's what i wrote first),
                    so feel free to take a look at it but don't expect anything to work"
                </Menu>
            </Menu>
            <Menu title="stuff i like">
                <Menu title="video games">
                    <Menu
                        title="Outer Wilds"
                        href="https://www.mobiusdigitalgames.com/outer-wilds.html"
                    >
                        <QuantumPoem rows={
                            let mut rows = [
                                "In the ancient glade".to_string(),
                                "Across old bark".to_string(),
                                "The quiet shade".to_string(),
                                "It's always dark".to_string(),
                            ];
                            let mut rng = thread_rng();
                            rows.shuffle(&mut rng);
                            rows
                        }/>
                        "this might honestly be the best game i ever played"
                        "it really is a one of a kind experience"
                        "just trust me and play it, and please don't spoil it for yourself in any way"
                    </Menu>
                </Menu>
                <Menu title="music">
                    <Menu title="Grand Commander" href="https://grandcommander.bandcamp.com/">
                        "one of my favorite artists ever, his music is so cozy but also so validating
                        and it just makes me feel a certain way that feels just right"
                        <Menu title="good songs!!">
                            <A href="https://grandcommander.bandcamp.com/track/journey-to-wherever-we-may-go">
                                "Journey to Wherever We May Go"
                            </A>
                            <A href="https://grandcommander.bandcamp.com/track/fuck-you-im-going-underground">
                                "fuck you i'm going underground"
                            </A>
                            <A href="https://grandcommander.bandcamp.com/track/i-kicked-a-hole-in-your-fence">
                                "I Kicked a Hole In Your Fence"
                            </A>
                        </Menu>
                    </Menu>
                    <Menu title="lvrby" href="https://www.youtube.com/@lvrby4868">
                        "also one of my favorite artists i think definitely, i'm not good with words but he's amazing,
                        please listen to him"
                        <Menu title="very good songs">
                            <A href="https://www.youtube.com/watch?v=RliWUpaAAmE">"Who Am I"</A>
                            <A href="https://www.youtube.com/watch?v=bhjUolCrMlM">"7.12.19"</A>
                        </Menu>
                    </Menu>
                </Menu>
                <Menu title="other stuff??">
                    <Menu title="Lost Terminal" href="https://lostterminal.com/">
                        "idk what to say (why am i even trying to write stuff in the first place),
                        but this is so amazing, i love this story with all my heart (or something like that),
                        but it's so hard to describe whyyyyyyy"
                        "just give it a chance and listen to it, maybe you'll love it too"
                    </Menu>
                </Menu>
            </Menu>
        </div>
    }
}

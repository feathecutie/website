{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    fenix = {
      url = "github:nix-community/fenix/monthly";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, crane, fenix, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        fenixPkgs = fenix.packages.${system};
        toolchain = fenixPkgs.fromToolchainFile {
          dir = ./.;
          sha256 = "sha256-gRj84JGU26rJnMfzlfmZw2BmBhcZft+GfoAUHx+okco=";
        };
        craneLib = (crane.mkLib pkgs).overrideToolchain toolchain;
        src = ./.;
        npmDeps = pkgs.fetchNpmDeps {
          inherit src;
          hash = "sha256-tPxlCg99DvyaENRwjQS8gKRsGUEuKrhaSrUVtjuMRwU=";
        };
        commonArgs = rec {
          pname = "feas-website";
          inherit src;
          strictDeps = true;
          buildInputs = [
            # Add additional build inputs here
            pkgs.cargo-leptos
            pkgs.binaryen
            pkgs.sass
            pkgs.cargo-generate
            pkgs.openssl
            pkgs.pkg-config
            pkgs.tailwindcss
            pkgs.nodejs
            pkgs.makeWrapper
          ];
          nativeBuildInputs = buildInputs;
        };
        cargoArtifacts = craneLib.buildDepsOnly (commonArgs // {
          doCheck = false;
        });
        my-app = craneLib.buildPackage (commonArgs // {
          buildPhaseCargoCommand = "cargo leptos build --release";
          inherit cargoArtifacts;
          preBuild = "npm ci --cache ${npmDeps} --prefer-offline";
          installPhaseCommand = ''
            mkdir -p $out/bin
            cp target/release/server $out/bin/feas-website
            cp -r target/site $out/bin/
            # cp Cargo.toml $out/bin/
            # wrapProgram $out/bin/feas-website \
            #   --set LEPTOS_OUTPUT_NAME "feas-website" \
            #   --set LEPTOS_SITE_ROOT "site" \
            #   --set LEPTOS_SITE_PKG_DIR "pkg" \
            #   --set LEPTOS_SITE_ADDR "127.0.0.1:3000" \
            #   --set LEPTOS_RELOAD_PORT "3001"
          '';
        });
      in
      {
        checks = {
          inherit my-app;
          my-app-clippy = craneLib.cargoClippy (commonArgs // {
            inherit cargoArtifacts;
            cargoClippyExtraArgs = "--all-targets -- --deny warnings";
          });
          my-app-fmt = craneLib.cargoFmt {
            inherit src;
          };
        };

        packages.default = my-app;

        apps.default = flake-utils.lib.mkApp {
          drv = my-app;
        };

        devShells.default = craneLib.devShell {
          checks = self.checks.${system};
          packages = [
            toolchain
            pkgs.trunk
            pkgs.bacon
            pkgs.leptosfmt
            fenixPkgs.rust-analyzer
            pkgs.nixpkgs-fmt
            pkgs.nil
            pkgs.cargo-leptos
            pkgs.nodejs
            pkgs.playwright-driver.browsers
          ];
          PLAYWRIGHT_BROWSERS_PATH = pkgs.playwright-driver.browsers;
          # PLAYWRIGHT_SKIP_VALIDATE_HOST_REQUIREMENTS = "true";
          # PLAYWRIGHT_NODEJS_PATH = "${pkgs.nodejs}/bin/node";
        };
      });
}
